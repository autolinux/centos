#!/bin/bash
# ---------------------------------------------------------------------------
# buildiso.sh - Create the environment and build a custom CentOS ISO

# Copyright 2016, Frederico Freire Boaventura <frederico@boaventura.net>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License at <http://www.gnu.org/licenses/> for
# more details.

# Usage: buildiso.sh [-h|--help] [-d|--download] [-b|--build 32|64] [-r|--refresh] [-f|--file ks.cfg]

# Revision history:
# 2016-10-19 Created by new_script ver. 3.3
# ---------------------------------------------------------------------------

PROGNAME=${0##*/}
VERSION="0.1"

##################
# Setup Variables
##################
COS_MIRROR='http://centos.ufes.br'
COS_VERSION='7'
COS_MACHINE='x86_64'
COS_TYPE='DVD'
COS_BUILD='1511'
COS_URL="${COS_MIRROR}/${COS_VERSION}/isos/${COS_MACHINE}/CentOS-${COS_VERSION}-${COS_MACHINE}-${COS_TYPE}-${COS_BUILD}.iso"
COS_SUM="${COS_MIRROR}/${COS_VERSION}/isos/${COS_MACHINE}/sha256sum.txt"

WORKDIR=$(dirname $(readlink -f $BASH_SOURCE))
IMAGEDIR="${WORKDIR}/isos"
CONFDIR="${WORKDIR}/ks"
SRCIMGDIR="${WORKDIR}/src_image"
SRCIMGFILE="CentOS-${COS_VERSION}-${COS_MACHINE}-${COS_TYPE}-${COS_BUILD}.iso"
SRCIMGSHA="sha256sum.txt"
BUILDDIR="${WORKDIR}/build"
BUILDISO="${BUILDDIR}/isolinux"
BUILDUTILS="${BUILDDIR}/utils"
BUILDPKGS="${BUILDISO}/Packages"
BUILDCONF="${BUILDISO}/ks";
BUILDIMG="${BUILDISO}/images"
BUILDLIVE="${BUILDISO}/LiveOS"
BUILDTREE="${BUILDDIR}/isolinux ${BUILDDIR}/utils ${BUILDISO}/repodata ${BUILDISO}/images \
    ${BUILDISO}/ks ${BUILDISO}/LiveOS ${BUILDISO}/Packages ${SRCIMGDIR}"
 

IMGNAME="InnoVM ${COS_MACHINE}"
#IMGLABEL="$(echo $IMGNAME | sed -e 's/ /\\x20/g')"
IMGLABEL="${IMGNAME// /\\x20}"
IMGFILE="${IMGNAME// /-}.iso"
IMGAUTHOR="Frederico Freire Boaventura"

LOGREDIR=" 1>/dev/null 2>&1"
NEEDDOWNLOAD="0"

#####################################################################################
### DON'T TOUCH ANYTHING BELLOW HERE, UNLESS YOU KNOW WHAT YOU ARE DOING!!!
#####################################################################################

clean_up() { # Perform pre-exit housekeeping
  ${FUSERMOUNT} -u ${SRCIMGDIR} 1>/dev/null 2>&1
  return
}

error_exit() {
  echo -e "${PROGNAME}: ${1:-"Unknown Error"}" >&2
  clean_up
  exit 1
}

graceful_exit() {
  clean_up
  exit
}

signal_exit() { # Handle trapped signals
  case $1 in
    INT)
      error_exit "Program interrupted by user" ;;
    TERM)
      echo -e "\n$PROGNAME: Program terminated" >&2
      graceful_exit ;;
    *)
      error_exit "$PROGNAME: Terminating on unknown signal" ;;
  esac
}

usage() {
  echo -e "Usage: $PROGNAME [-h|--help] [-f|--file ks.cfg]"
}

help_message() {
  cat <<- _EOF_
  $PROGNAME ver. $VERSION
  Create the environment and build a custom CentOS ISO

  $(usage)

  Options:
  -h, --help  Display this help message and exit.
  -f, --file  File to be used as seeded conf

  NOTE: You must be the superuser to run this script.

_EOF_
  return
}

fnoksn() {
    if [ "$1" -eq 0 ]; then
        echo -en "[OK]\n"
        return 0
    else
        echo -en "[NOK]\n"
        return 127
    fi
}

check_deps() {
    # Check needed programs
    FUSEISO=$(which fuseiso)
    [[ -z ${FUSEISO} ]] && PKGINST="${PKGINST} fuseiso"
    MKISOFS=$(which mkisofs)
    [[ -z ${MKISOFS} ]] && PKGINST="${PKGINST} genisoimage"
    FUSERMOUNT=$(which fusermount)
    [[ -z ${FUSERMOUNT} ]] && PKGINST="${PKGINST} fuse"
    CURL=$(which curl)
    [[ -z ${CURL} ]] && PKGINST="${PKGINST} curl"
    SHA256SUM=$(which sha256sum)
    [[ -z ${SHA256SUM} ]] && PKGINST="${PKGINST} sha256sum" 
    ZCAT=$(which zcat)
    [[ -z ${ZCAT} ]] && PKGINST="${PKGINST} gzip"
    GZIP=$(which gzip)
    [[ -z ${GZIP} ]] && PKGINST="${PKGINST} gzip"
    CREATEREPO=$(which createrepo)
    [[ -z ${CREATEREPO} ]] && PKGINST="${PKGINST} createrepo"

    if [ -n "${PKGINST}" ]; then
        echo -en ".: The following packages are needed! ${PKGINST}\n"
        read -rp "..: Shall I install them for you? [y/n] " answeryn
        if [[ ${answeryn} =~ ^[yY]$ ]]; then
            pkg_install
            PKGINST=
        else
            error_exit "..: OK! Packages not being installed..."
        fi
    fi

    return 0
}

pkg_install() {
    yum update -q -y
    yum install -q -y ${PKGINST}

    echo -en "..: Packages sucessfuly installed!\n"
}

build_tree() {
    if [ -d ${BUILDDIR} ] && [ -d ${BUILDISO} ]; then
        read -rp ".: There is already a build folder, shall I remove it and start fresh? [y/n] " answeryn
        if [[ ${answeryn} =~ ^[yY]$ ]]; then
            rm -rf ${BUILDISO}
            echo -en ".: Creating the building folder tree... "
            mkdir -p ${BUILDTREE}
            fnoksn $?
        else
            echo -en "..: OK! Leaving the build folder alone...\n"
        fi
    else
        echo -en ".: Creating the building folder tree... "
        mkdir -p ${BUILDTREE}
        fnoksn $?
    fi
}

check_source() {
    echo -en ".: Checking if the ISO is already downloaded...\n"
    if [ ! -d ${WORKDIR}/isos ]; then
        mkdir -p ${WORKDIR}/isos
        NEEDDOWNLOAD=1
    elif [ -f "${IMAGEDIR}/${SRCIMGFILE}" ]; then
        echo -en "..: ISO file found! Checking if it's sane... "
        if [ ! -f "${IMAGEDIR}/${SRCIMGSHA}" ]; then
            ${CURL} -so ${IMAGEDIR}/${SRCIMGSHA} ${COS_SUM}
        fi

        cd "${IMAGEDIR}" || error_exit "..: Failed to enter ${IMAGEDIR} folder!"
        egrep ${SRCIMGFILE} ${SRCIMGSHA} | ${SHA256SUM} -c -
        if [ $? -eq 0 ]; then
            echo -en "[OK]\n"
            NEEDDOWNLOAD=0
        else
            echo -en "[NOK]\n"
            NEEDDOWNLOAD=1
        fi
    else
        echo -en "[NOK]\n"
        NEEDDOWNLOAD=1
    fi
}

download_image() {

    if [ -z "${NEEDDOWNLOAD}" ] || [ ${NEEDDOWNLOAD} -ne 1 ]; then
        check_source
    fi

    [[ ! -f "${IMAGEDIR}/${SRCIMGFILE}" ]] && check_source

    if [ ${NEEDDOWNLOAD} -eq 1 ]; then
        echo -e ".: Downloading ${SRCIMGFILE}... It may take a while, depending on your connection!"
        ${CURL} -so  ${IMAGEDIR}/${SRCIMGFILE} "${COS_URL}" 1>/dev/null 2>&1
        ${CURL} -so ${IMAGEDIR}/${SRCIMGSHA} "${COS_SUM}" 1>/dev/null 2>&1

        echo -en "..: Verifying if the download was sucessful... "
        check_source
        if [ "${NEEDDOWNLOAD}" -eq 0 ]; then
            echo -en "[OK]\n"
        else
            echo -en "[NOK]\n"
            echo -en "..: The download has failed! Please check and try again...\n"
            echo -en "..: In case you want to try downloading manually, the URL is: \n"
            echo -en "..::        ${COS_URL}\n"
            exit 127
        fi
    fi
}

populate_image() {
    build_tree
    download_image
    echo -en ".: Mounting Source ISO to folder... "
    ${FUSEISO} ${IMAGEDIR}/${SRCIMGFILE} ${SRCIMGDIR}
    fnoksn $?
    [[ $? -ne 0 ]] && error_exit ".: Mounting of the image failed!\n"

    echo -en ".: Copying files from source ISO to build folder (you can go grab a coke)\n"
    echo -en "..: Copying isolinux... "
    cp -R ${SRCIMGDIR}/isolinux/* ${BUILDISO}/ 1>/dev/null 2>&1
    fnoksn $?
    [[ $? -ne 0 ]] && error_exit "[NOK]"
    echo -en "..: Copying discinfo... "
    cp -R ${SRCIMGDIR}/.discinfo ${BUILDISO}/ 1>/dev/null 2>&1
    fnoksn $?
    [[ $? -ne 0 ]] && error_exit "[NOK]"
    echo -en "..: Copying images... "
    cp -R ${SRCIMGDIR}/images/* ${BUILDISO}/images/ 1>/dev/null 2>&1
    fnoksn $?
    [[ $? -ne 0 ]] && error_exit "[NOK]"
    echo -en "..: Copying LiveOS... "
    cp -R ${SRCIMGDIR}/LiveOS/* ${BUILDISO}/LiveOS 1>/dev/null 2>&1
    fnoksn $?
    [[ $? -ne 0 ]] && error_exit "[NOK]"
    echo -en "..: Copying Packages... "
    cp -R ${SRCIMGDIR}/Packages/* ${BUILDISO}/Packages 1>/dev/null 2>&1
    fnoksn $?
    [[ $? -ne 0 ]] && error_exit "[NOK]"
    echo -en "..: Copying repodata... "
    cp ${SRCIMGDIR}/repodata/*-${COS_MACHINE}-comps.xml.gz ${BUILDDIR}/comps.xml.gz 1>/dev/null 2>&1
    fnoksn $?
    [[ $? -ne 0 ]] && error_exit "[NOK]"
    echo -en "..: Copying ks.cfg... "
    if [ -f ${COS_KSFILE} ]; then
        cp -R "${COS_KSFILE}" ${BUILDISO}/ks/ks.cfg 1>/dev/null 2>&1
        fnoksn $?
        [[ $? -ne 0 ]] && error_exit "[NOK]"
    elif [ -f ${CONFDIR}/${COS_KSFILE} ]; then
        cp -R "${CONFDIR}/${COS_KSFILE}" ${BUILDISO}/ks/ks.cfg 1>/dev/null 2>&1
        fnoksn $?
        [[ $? -ne 0 ]] && error_exit "[NOK]"
    elif [ -f "${WORKDIR}/${COS_KSFILE}" ]; then
        cp -R "${WORKDIR}/${COS_KSFILE}" ${BUILDISO}/ks/ks.cfg 1>/dev/null 2>&1
        fnoksn $?
        [[ $? -ne 0 ]] && error_exit "[NOK]"
    else
        error_exit "File not found! Please, make sure that your file is at ${WORKDIR} or ${CONFDIR}"
    fi
    echo -en "..: Finished the copy process!\n"
}

rpm_prep() {
    echo -en ".: Prepare RPM package database... \n"
    echo -en "..:   Make sure that all the packages you want to be copied, that are not part of default CentOS installation\n"
    echo -en "..:    are placed inside ${WORKDIR}/extra_packages with all its dependencies met\n"
    cp -R ${WORKDIR}/extra ${BUILDISO}/ 1>/dev/null 2>&1
    fnoksn $?
    [[ $? -ne 0 ]] && error_exit "Failed to copy extra packages"

    echo -en "..: Creating the repository data... "
    cd ${BUILDISO}
    ${GZIP} -f -d ${BUILDDIR}/comps.xml.gz
    cp ${BUILDDIR}/comps.xml ${BUILDISO}/repodata/
    ${CREATEREPO} -g repodata/comps.xml .
    fnoksn $?
    [[ $? -ne 0 ]] && error_exit "Failed to create packages repository data"
}

build_image() {

    rpm_prep

    cp ${WORKDIR}/cdfiles/isolinux.cfg ${BUILDISO}/isolinux.cfg
    cp ${WORKDIR}/cdfiles/splash.png ${BUILDISO}/splash.png
    cp 
    cd "${BUILDDIR}"
    chmod 644 ${BUILDISO}/isolinux.bin
    [[ -f "${IMAGEDIR}/${IMGFILE}" ]] && rm -f ${IMAGEDIR}/${IMGFILE}
    mkisofs -o ${IMAGEDIR}/${IMGFILE} -b isolinux.bin -c boot.cat -no-emul-boot \
          -V "${IMGNAME}" \
            -boot-load-size 4 -boot-info-table -R -J -v -T isolinux/
}

# Trap signals
trap "signal_exit TERM" TERM HUP
trap "signal_exit INT"  INT

# Check for root UID
if [[ $(id -u) != 0 ]]; then
  error_exit "You must be the superuser to run this script."
fi

# Check for dependencies
check_deps

if [[ -z $1 ]]; then
    usage
    graceful_exit
fi

# Parse command-line
while [[ -n $1 ]]; do
  case $1 in
    -h | --help)
      help_message; graceful_exit ;;
    -f | --file)
        shift; COS_KSFILE=${1:-"ks.cfg"}
        ;;
    -* | --*)
      usage
      error_exit "Unknown option $1" ;;
    *)
      echo "Argument $1 to process..." ;;
  esac
  shift
done

# Main logic


populate_image

build_image

graceful_exit

